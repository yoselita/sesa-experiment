This repository contains files, programs and scripts needed to set the WRF atmospheric model for simulating 3-years period over sub-South-American CORDEX domain on the convection permitting (CP) scale. 

The repository is organized in folders:

1. "Settings" folder contains namelists for running WPS and WRF, as well as domain info and geo_em.d01 file.

2. "Preprocessing" folder contains scripts and the complete procedure how to update soil state in the wrfinput file from an available WRF output file (in concrete case from wrf CP run conducted in NCAR), to avoid long spinup.

