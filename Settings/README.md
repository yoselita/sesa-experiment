Description of the files necessary to run WPS (v4.3) and WRF (v4.3.3) model:

namelist.wps is used to run WPS. 

geo_em.d01.pdf shows the doman size (to plot it run: plot_domain.py geo_em.d01.nc). The grey area represents 20 grids that are considered as relaxation zone, and the domain within the red boudaries show the obligatory domain as described in the extended [FPS-SESA protocol](https://docs.google.com/document/d/1PEYRxzSNGOUmXL4Symz-_cyJt2h8zRv0Szc_PEyIUec/edit#heading=h.1op7z4an4mql).
